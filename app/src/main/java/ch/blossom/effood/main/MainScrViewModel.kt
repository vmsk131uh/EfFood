package ch.blossom.effood.main

import android.app.Application
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.Snapshot
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import ch.blossom.effood.data.DataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

private const val TAG = "MainScrViewModel"
class MainScrViewModel(val apply: Application) : AndroidViewModel(apply) {
    val listOfCategories = DataRepository.listOfCategories

    var showProgressIndicator by mutableStateOf(false)
    private fun fetchJson() {
        if(listOfCategories.isNotEmpty())
            return
        showProgressIndicator=true
        viewModelScope.launch(Dispatchers.IO) {

            if(withContext(Dispatchers.Main){listOfCategories.isEmpty()})
                DataRepository.refreshListOfCategories()

            withContext(Dispatchers.Default){
                Snapshot.withMutableSnapshot {
                    showProgressIndicator=false
                }
            }
//            val l = FetchJson.fetchCategory()
//            when (l) {
//                is FetchResponse.Error -> {
//                    snackbarHostState.showSnackbar("${l.exception}\n${l.message}")
//                }
//
//                else -> {
//                    Log.v(TAG,"fetchJson ELSE!!!!")
//                }
//            }
//            Log.v(TAG, "Fetching Category: $l")
        }
    }
    init {
        fetchJson()
    }
}