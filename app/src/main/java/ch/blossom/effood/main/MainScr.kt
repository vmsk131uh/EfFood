package ch.blossom.effood.main

import android.util.Log
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import ch.blossom.fetch.CategoriesJson
import coil.compose.AsyncImage

@Composable
fun MainScr(
    navController: NavController,
    navigateToDishes: (String) -> Unit
) {

//    Log.v(TAG, "MainScr: ----------------------------")
//    navController.currentBackStack.collectAsState().value.forEach {
//        Log.v(TAG, "MainScr: \n$it")
//    }
//    Log.v(TAG, "MainScr: ----------------------------")

    Log.v(
        TAG,
        "^^^^^^^MainScr: navController.currentDestination?.route=${navController.currentDestination?.route}"
        //"^^^^^^^MainScr: navController.currentDestination?.route=${navController.currentBackStack.collectAsState()}"
    )
    val vm = viewModel<MainScrViewModel>()
    val scope = rememberCoroutineScope()

    LazyColumn {
        items(
            vm.listOfCategories
        ) {
            OneCategory(category = it, goToCatId = navigateToDishes)
        }
    }

    if (vm.showProgressIndicator)
        Box(Modifier.fillMaxSize(), Alignment.Center) {
            CircularProgressIndicator()
        }
}

@Composable
fun OneCategory(
    category: CategoriesJson.Category,
    goToCatId: (String) -> Unit
) {
    Surface(
        {
            Log.v(TAG, "clicked food category: id=${category.id}, name=${category.name}")
            goToCatId(category.id.toString())
        }
        /*        {
                    Log.v(TAG, "clicked food category: id=${category.id}, name=${category.name}")
                    navController.navigate(
                        //Screen.Main.route +
                            "main?catId=${category.id}") {
                        popUpTo(navController.graph.findStartDestination().id) { saveState = true }
                        launchSingleTop = true
                        restoreState = true

                    }
                }*/,
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp, 8.dp, 16.dp, 8.dp)
    ) {
        AsyncImage(
            model = category.imageUrl, contentDescription = category.name,
            //modifier = Modifier.fillMaxWidth().padding(16.dp),
            contentScale = ContentScale.FillWidth
        )
        category.name?.let {
            Row {
                Text(
                    it,
                    Modifier
                        .padding(16.dp)
                        .weight(1f),
                    style = MaterialTheme.typography.titleLarge,
                )
                Spacer(modifier = Modifier.weight(1f))
            }
        }
    }
}



private const val TAG = "MainScr"