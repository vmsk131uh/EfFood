package ch.blossom.effood.cart

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import ch.blossom.effood.data.DataRepository

class CartViewModel(val apply: Application) : AndroidViewModel(apply) {

    val cart = DataRepository.cart
    val dishes = DataRepository.listOfDishes
}
