package ch.blossom.effood.cart

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilterChip
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import ch.blossom.effood.data.monFormat
import coil.compose.AsyncImage


@Composable
fun CartScr(navController: NavController) {
    val vm = viewModel<CartViewModel>()

    Column {


        LazyColumn(
            Modifier
                .fillMaxSize()
                .weight(1f)) {
            items(
                items = vm.cart.toList()
            ) {
                val i = vm.dishes.indexOfFirst { d -> d.id == it.first }
                if (i != -1) {
                    val dish = vm.dishes[i]
                    Row(
                        Modifier
                            .fillMaxWidth()
                            .padding(8.dp),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceAround
                    ) {
                        Surface(
                            Modifier.size(96.dp, 96.dp),
                            tonalElevation = 4.dp,
                            shadowElevation = 4.dp,
                            shape = MaterialTheme.shapes.small
                        ) {
                            AsyncImage(model = dish.imageUrl, contentDescription = dish.name)
                        }
                        Column(
                            Modifier
                                .weight(1f)
                                .padding(8.dp)) {
                            Text("${dish.name}") //: ${it.second} P")
                            Row {
                                Text("${monFormat.format(dish.price)} ")
                                Text("${dish.weight}г", color = Color.Gray)
                            }

                        }
                        PlusMinusBtn(it.second) { plusOrMimus: PlusMinusEnum ->
                            when (plusOrMimus) {
                                PlusMinusEnum.PLUS -> {
                                    vm.cart.replace(it.first, it.second + 1)
                                }

                                PlusMinusEnum.MINUS -> {
                                    if (it.second > 1)
                                        vm.cart.replace(it.first, it.second - 1)
                                    else
                                        vm.cart.remove(it.first)
                                }
                            }

                        }
                    }
                }
            }
        }
        val summa = vm.cart.map { c -> vm.dishes[vm.dishes.indexOfFirst { d -> d.id == c.key }].price?.times(c.value) ?: 0 }.sum()
        val str: String = monFormat.format(summa)
        Button(onClick = { /*TODO*/ },Modifier.align(Alignment.CenterHorizontally)) {
            Text("Оплатить $str")
        }
    }
}

enum class PlusMinusEnum {
    PLUS, MINUS
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PlusMinusBtn(amount: Int = 0, plusMinusCallback: (PlusMinusEnum) -> Unit = { }) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        FilterChip(
            selected = true,
            {
                plusMinusCallback(PlusMinusEnum.MINUS)
            },
            label = {
                Text(
                    "-",
                    fontSize = TextUnit(24f, TextUnitType.Sp),
                    fontWeight = FontWeight.Bold
                )
            },
            shape = MaterialTheme.shapes.small.copy(
                topEnd = CornerSize(0.dp),
                bottomEnd = CornerSize(0.dp)
            ),
        )

        Text(
            String.format("% 3d", amount),
            Modifier.padding(0.dp, 0.dp, 4.dp, 0.dp),
            fontFamily = FontFamily.Monospace,
            fontWeight = FontWeight.Bold
        )
        FilterChip(
            selected = true,
            onClick = {
                plusMinusCallback(PlusMinusEnum.PLUS)
            },
            label = {
                Text(
                    "+",
                    fontSize = TextUnit(24f, TextUnitType.Sp)
                )
            },
            shape = MaterialTheme.shapes.small.copy(
                topStart = CornerSize(0.dp),
                bottomStart = CornerSize(0.dp)
            )
        )


    }
}

@Preview
@Composable
fun ViewPlusMinus() {
    PlusMinusBtn()
}