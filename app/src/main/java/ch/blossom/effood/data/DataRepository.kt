package ch.blossom.effood.data

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.snapshots.Snapshot
import ch.blossom.effood.snackbarHostState
import ch.blossom.fetch.CategoriesJson
import ch.blossom.fetch.DishesJson
import ch.blossom.fetch.FetchJson
import ch.blossom.fetch.FetchResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DataRepository {
    companion object {

        var cart = mutableStateMapOf<Int, Int>()

        var listOfCategories = mutableStateListOf<CategoriesJson.Category>()

        suspend fun refreshListOfCategories() {
            var r = FetchJson.fetchCategory()
            if (r is FetchResponse.Success<*>) {
                updateCategoryListExtr(r)
            } else if (r is FetchResponse.Error) {
                r = FetchJson.fetchCategory()
                if (r is FetchResponse.Success<*>) {
                    updateCategoryListExtr(r)
                } else if (r is FetchResponse.Error) {
                    snackbarHostState.showSnackbar("${r.exception}\n${r.message}")
                } else if (r is FetchResponse.HtmlError) {
                    snackbarHostState.showSnackbar("${r.code} ${r.message}\n${r.body}")
                }
            }
        }

        private suspend fun updateCategoryListExtr(r: FetchResponse) {
            withContext(Dispatchers.Default) {
                Snapshot.withMutableSnapshot {
                    listOfCategories.clear()
                    ((r as FetchResponse.Success<*>).resp as List<*>).forEach { c: Any? ->
                        if (c is CategoriesJson.Category) {
                            listOfCategories.add(c)
                        }
                    }
                }
            }
        }

        var listOfDishes = mutableStateListOf<DishesJson.Dish>()
        val listOfTegs = mutableStateListOf<TegSelection>()
        suspend fun refreshListOfDishes() {
            var r = FetchJson.fetchDishes()
            if (r is FetchResponse.Success<*>) {
                updateDishesListExtr(r)
            } else if (r is FetchResponse.Error) {
                r = FetchJson.fetchDishes()
                if (r is FetchResponse.Success<*>) {
                    updateDishesListExtr(r)
                } else if (r is FetchResponse.Error) {
                    snackbarHostState.showSnackbar("${r.exception}\n${r.message}")
                } else if (r is FetchResponse.HtmlError) {
                    snackbarHostState.showSnackbar("${r.code} ${r.message}\n${r.body}")
                }
            }
        }

        private suspend fun updateDishesListExtr(r: FetchResponse) {
            withContext(Dispatchers.Default) {
                Snapshot.withMutableSnapshot {
                    listOfDishes.clear()
                    ((r as FetchResponse.Success<*>).resp as List<*>).forEach { c: Any? ->
                        if (c is DishesJson.Dish) {
                            listOfDishes.add(c)
                        }
                    }
                    listOfTegs.clear()
                    listOfTegs.addAll(
                        (listOfDishes.flatMap { it.tegs ?: listOf() }.filter { !it.isNullOrBlank() }
                            .toSet().toList() as List<String>).map { TegSelection(it, false) }
                    )
                    if(listOfTegs.isNotEmpty())
                        listOfTegs[0] = listOfTegs[0].copy(selected = true)
                }
            }
        }

    }
}

data class TegSelection(
    var teg: String,
    var selected: Boolean
)
