package ch.blossom.effood.data

import android.app.Application
import android.content.Context
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import java.text.NumberFormat
import java.util.Locale

val Context.dataStore by preferencesDataStore("datastore_data")

val categoryKey = stringPreferencesKey("category")

val monFormat: NumberFormat = NumberFormat.getCurrencyInstance(Locale("ru", "RU")).apply {
    maximumFractionDigits = 0
}

class ElFoodApps: Application() {
    companion object {
        lateinit var elFoodApps: ElFoodApps
    }
    override fun onCreate() {
        super.onCreate()
        elFoodApps=this
    }

}

