package ch.blossom.effood.search

import android.app.Application
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.AndroidViewModel

class SeachViewModel(val apply: Application) : AndroidViewModel(apply) {
    var searchString by mutableStateOf("")
}
