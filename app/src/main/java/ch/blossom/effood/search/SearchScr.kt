package ch.blossom.effood.search

import android.util.Log
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import ch.blossom.effood.snackbarHostState
import kotlinx.coroutines.launch

private const val TAG = "SearchScr"
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchScr(navController: NavController) {
    val vm = viewModel<SeachViewModel>()
    val scope = rememberCoroutineScope()

    Column {
        TextField(
            value = vm.searchString,
            onValueChange = { vm.searchString = it },
            label = { Text("Search") })
        Button({
            Log.v(TAG, "SearchScr: searching ${vm.searchString}")
            scope.launch {
                snackbarHostState.showSnackbar("Searching \"${vm.searchString}\" string")
            }
        }){Text("SEARCH")}
    }
}