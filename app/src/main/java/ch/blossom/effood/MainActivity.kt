package ch.blossom.effood

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.KeyboardArrowLeft
import androidx.compose.material.icons.outlined.LocationOn
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.graphics.drawable.toBitmap
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import androidx.navigation.navigation
import ch.blossom.effood.account.AccountScr
import ch.blossom.effood.cart.CartScr
import ch.blossom.effood.data.DataRepository
import ch.blossom.effood.main.MainScr
import ch.blossom.effood.dishes.DishesScr
import ch.blossom.effood.search.SearchScr
import ch.blossom.effood.ui.theme.EfFoodTheme
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            EfFoodTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    EfApp()
                }
            }
        }
    }
}


private const val TAG = "MainActivity"

sealed class Screen(val route: String, @StringRes val labelResourceId: Int, @DrawableRes val iconResourceId: Int) {
    object Dishes : Screen(DISHES_GRAPH_ROUTE, R.string.main, R.drawable.outline_home_24) {
         fun categoryId(
            id: String = "-1"
        ): String {
            return "dishes?catId=$id"
        }
    }
    object Main : Screen(MAIN_GRAPH_ROUTE, R.string.main, R.drawable.outline_home_24)
    object Search : Screen(SEARCH_GRAPH_ROUTE, R.string.search, R.drawable.baseline_search_24)
    object Cart : Screen(CART_GRAPH_ROUTE, R.string.cart, R.drawable.outline_shopping_cart_24)
    object Account : Screen(ACCOUNT_GRAPH_ROUTE, R.string.account, R.drawable.outline_account_circle_24)
}

private const val HOME_GRAPH_ROUTE = "home"
private const val MAIN_GRAPH_ROUTE = "main"
private const val SEARCH_GRAPH_ROUTE = "search"
private const val CART_GRAPH_ROUTE = "cart"
private const val ACCOUNT_GRAPH_ROUTE = "account"
private const val DISHES_GRAPH_ROUTE = "dishes?catId={catId}"

sealed class NavBarItem(val route: String, val selectedRoutes: List<String>, @StringRes val labelResourceId: Int, @DrawableRes val iconResourceId: Int) {
    object Home : NavBarItem(
        HOME_GRAPH_ROUTE,
        listOf(Screen.Main.route, Screen.Dishes.route),
        Screen.Main.labelResourceId,
        Screen.Main.iconResourceId
    )

    object Search : NavBarItem(
        SEARCH_GRAPH_ROUTE,
        listOf(Screen.Search.route),
        Screen.Search.labelResourceId,
        Screen.Search.iconResourceId
    )

    object Cart : NavBarItem(
        CART_GRAPH_ROUTE,
        listOf(Screen.Cart.route),
        Screen.Cart.labelResourceId,
        Screen.Cart.iconResourceId
    )

    object Account : NavBarItem(
        ACCOUNT_GRAPH_ROUTE,
        listOf(Screen.Account.route),
        Screen.Account.labelResourceId,
        Screen.Account.iconResourceId
    )
}

val navBarItemsList = listOf(
    NavBarItem.Home,
    NavBarItem.Search,
    NavBarItem.Cart,
    NavBarItem.Account,
)

val snackbarHostState = SnackbarHostState()

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EfApp(modifier: Modifier = Modifier) {
    val scope = rememberCoroutineScope()
    val routeMenu = stringArrayResource(id = R.array.routeMenus)

    val navController = rememberNavController()
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute = navBackStackEntry?.destination?.route

    Scaffold(
        bottomBar = {
            NavigationBar() {
                navBarItemsList.forEachIndexed { i: Int, navBarItem: NavBarItem ->
                    NavigationBarItem(
                        selected = navBarItem.selectedRoutes.any { it == currentRoute},
                        onClick = {
                            navController.navigate(navBarItem.route) {
                                // Pop up to the start destination of the graph to
                                // avoid building up a large stack of destinations
                                // on the back stack as users select items
                                popUpTo(navController.graph.findStartDestination().id) {
                                    saveState = true
                                }
                                // Avoid multiple copies of the same destination when
                                // reselecting the same item
                                launchSingleTop = true
                                // Restore state when reselecting a previously selected item
                                restoreState = true
                            }
                        },
                        icon = { Icon(painterResource(navBarItem.iconResourceId), null) },
                        label = { Text(stringResource(navBarItem.labelResourceId)) }
                    )

                }
            }
        },
        topBar = {
            TopAppBar(
                title = {
                    if (navController.currentDestination?.route == Screen.Dishes.route) {
                        (navBackStackEntry?.arguments?.getString("catId")?.toIntOrNull())?.minus(1)?.let {
                            Text("${DataRepository.listOfCategories[it].name}")
                        }?:Text("El Food")
                    } else {
                        Column {
                            Text("Москва")
                            Text(
                                LocalDate.now()
                                    .format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)),
                                style = MaterialTheme.typography.labelSmall,
                                color = Color.Gray
                            )
                        }
                    }
                },
                actions = {
                    val drawable =
                        AppCompatResources.getDrawable(LocalContext.current, R.drawable.iconpic)
                    drawable?.let {
                        Image(it.toBitmap().asImageBitmap(), null)
                    }
                },
                navigationIcon = {
                    if (navController.currentDestination?.route != Screen.Dishes.route) {
                        IconButton(onClick = {
                        }) {
                            Icon(Icons.Outlined.LocationOn, null)
                        }
                    } else {
                        IconButton(onClick = {
                            Log.v(
                                TAG,
                                "Greeting: navController.navigate(Screen.Main.route=${Screen.Main.route})"
                            )
                            navController.popBackStack()
//                            navController.navigate(Screen.Main.route) {
//                                // Pop up to the start destination of the graph to
//                                // avoid building up a large stack of destinations
//                                // on the back stack as users select items
//                                popUpTo(navController.graph.findStartDestination().id) {
//                                    saveState = true
//                                }
//                                // Avoid multiple copies of the same destination when
//                                // reselecting the same item
//                                launchSingleTop = true
//                                // Restore state when reselecting a previously selected item
//                                //restoreState = true
//                            }
                        }) {
                            Icon(Icons.Outlined.KeyboardArrowLeft, null)
                        }
                    }


                }

            )
        },
        snackbarHost = {
            SnackbarHost(
                hostState = snackbarHostState,
                modifier = Modifier.imePadding()
            )
        }
    ) { paddingValues: PaddingValues ->
        NavHost(
            navController = navController,
            startDestination = HOME_GRAPH_ROUTE, //Screen.Main.route,
            Modifier.padding(paddingValues)
        ) {

            navigation(startDestination = Screen.Main.route,route = HOME_GRAPH_ROUTE) {
                composable(
                    Screen.Dishes.route,
                    listOf(
                        navArgument("catId") {
                            nullable = true
                        }
                    ),
                ) { navBackStackEntry1: NavBackStackEntry ->
                    DishesScr(
                        navController,
                        navBackStackEntry1.arguments?.getString("catId")
                    )

                }
                /*            composable(Screen.Main.route) {
                MainScr(navController) {
                    navController.navigate(ch.blossom.effood.Screen.Main.route) {
                        // Pop up to the start destination of the graph to
                        // avoid building up a large stack of destinations
                        // on the back stack as users select items
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        // Avoid multiple copies of the same destination when
                        // reselecting the same item
                        launchSingleTop = true
                        // Restore state when reselecting a previously selected item
                        restoreState = true
                    }
                }
            }*/

                // navigation(startDestination = "categories?catId={catId}", route = Screen.Main.route) {
                composable(
                    Screen.Main.route,

                    ) { navBackStackEntry1 ->

/*                Column {
                    Text(navBackStackEntry1.arguments?.getString("catId") ?: "No catId")
                    Button(onClick = {
                        navController.navigate("main?catId=2") {
                            launchSingleTop = true
                            popUpTo(navController.graph.findStartDestination().id) {
                                saveState = true
                            }
                            restoreState = true
                        }
                    }) {
                        Text("Navigate to main?catId=2")
                    }
                    Button(onClick = {
                        navController.navigate("main?catId=4") {
                            launchSingleTop = true
                            popUpTo(navController.graph.findStartDestination().id) {
                                saveState = true
                            }
                            restoreState = true
                        }
                    }) {
                        Text("Navigate to main?catId=4")
                    }
                    Button(onClick = {
                        navController.navigate("main?catId={catId}") {
                            launchSingleTop = true
                            popUpTo(navController.graph.findStartDestination().id) {
                                saveState = true
                            }
                            restoreState = true
                        }
                    }) {
                        Text("Navigate to main?catId={catId}")
                    }
                }*/
                    MainScr(
                        navController,

                        ) { categoryId: String ->
                        //Log.v(TAG, "Greeting: navController.navigate(\"main?catId=$categoryId\")")
                        navController.navigate(Screen.Dishes.categoryId(categoryId)) {
                            // Pop up to the start destination of the graph to
                            // avoid building up a large stack of destinations
                            // on the back stack as users select items
                            //popUpTo(navController.graph.findStartDestination().id) {
                            //    saveState = true
                            //}
                            // Avoid multiple copies of the same destination when
                            // reselecting the same item
                            launchSingleTop = true
                            // Restore state when reselecting a previously selected item
                            restoreState = true
                        }
                    }
                }
            }
/*            }

            composable(
                Screen.Main.route + "?catId={catId}",
                listOf(navArgument("catId") {
                    nullable = true
                })
            ) {
                MainScr(
                    navController,
                    navBackStackEntry?.arguments?.getString("catId")
                ) {
                    navController.navigate(Screen.Main.route) {
                        // Pop up to the start destination of the graph to
                        // avoid building up a large stack of destinations
                        // on the back stack as users select items
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        // Avoid multiple copies of the same destination when
                        // reselecting the same item
                        launchSingleTop = true
                        // Restore state when reselecting a previously selected item
                        restoreState = true
                    }
                }
            }*/
            composable(Screen.Search.route) { SearchScr(navController) }
            composable(Screen.Cart.route) { CartScr(navController) }
            composable(Screen.Account.route) { AccountScr(navController) }

        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    EfFoodTheme {
        EfApp()
    }
}


/*
@Composable
fun App() {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = "home?arg={arg}"
    ) {
        composable(route = "home?arg={arg}") { backStackEntry ->
            val arg = backStackEntry.arguments?.getString("arg") ?: "Empty"
            Column {
                Text("Argument: $arg")
                Button(
                    onClick = {
                        navController.navigate("home?arg=value2") {
                            launchSingleTop = true
                        }
                    }
                ) {
                    Text("Click to navigate!")
                }
            }
        }
    }
}
*/
