package ch.blossom.effood.dishes

import android.util.Log
import android.util.Patterns
import android.webkit.URLUtil
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.AlertDialogDefaults
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilledTonalIconButton
import androidx.compose.material3.FilterChip
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import ch.blossom.effood.R
import ch.blossom.fetch.DishesJson
import coil.compose.AsyncImage

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DishesScr(navController: NavController, catId: String? = null) {
    Log.v(
        TAG,
        "^^^^^^^Menu(dishes)Scr: navController.currentDestination?.route=${navController.currentDestination?.route}"
    )
    val vm = viewModel<MenuViewModel>()
//
//    val listOfTegs = vm.listOfDishes.flatMap { it.tegs?:listOf() }.toSet().toList() as List<String>
//    //val listOfTegs = rememberSaveable(vm.listOfDishes) { vm.listOfDishes.flatMap { it.tegs?:listOf() }  } //.toSet().toList().filter { !it.isNullOrEmpty() } )}
//    Log.v(TAG,vm.listOfDishes.flatMap { it.tegs?:listOf() }.toString())
    when (catId) {
        "1", "2", "3", "4" -> {
            Column {
                LazyRow(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceAround) {
                    itemsIndexed(
                        //items = vm.listOfDishes.flatMap { it.tegs?:listOf() }.toSet().toList()  as List<String>,
                        items = vm.listOfTegs//.toList()
                    ) { i, teg ->

                        FilterChip(
                            selected = teg.selected,
                            onClick = {
                                val lidx = vm.listOfTegs.indexOfFirst { it.selected }
                                if (lidx != -1)
                                    vm.listOfTegs[lidx] = vm.listOfTegs[lidx].copy(selected = false)
                                vm.listOfTegs[i] = teg.copy(selected = true)
                                //teg.selected = !teg.selected
                            },
                            label = {
                                Text(teg.teg)
                            },
                            modifier = Modifier.padding(4.dp)
                        )

                    }
                }
                LazyVerticalGrid(
                    columns = GridCells.Fixed(3)
                ) {
                    items(
                        vm.listOfDishes.filter { dish ->
                            dish.tegs?.contains(
                                vm.listOfTegs.find { teg -> teg.selected }?.teg
                            ) ?: false
                        }
                    ) {
                        val t = vm.listOfTegs.find { t -> t.selected }?.teg
                        if (it.tegs?.contains(t) == true)
                            OneDish(
                                dish = it
                            ) { dishId: Int ->
                                val i = vm.listOfDishes.indexOfFirst { i -> i.id == dishId }
                                if (i != -1)
                                    vm.fillDishDialog(i)
                                vm.showDishDialog = true
                            }
                    }
                }
                if (vm.showDishDialog) {
                    AlertDialog(
                        onDismissRequest = {
                            vm.showDishDialog = false
                        },
                        modifier = Modifier
                            .wrapContentWidth()
                            .wrapContentHeight(),
                    ) {
                        Surface(
                            shape = MaterialTheme.shapes.large,
                            tonalElevation = AlertDialogDefaults.TonalElevation,
                        ) {
                            Column(Modifier.verticalScroll(rememberScrollState())) {
                                Card(
                                    Modifier
                                        .fillMaxWidth()
                                        .padding(16.dp),

                                    ) {
                                    Box(
                                        Modifier.fillMaxWidth(),
                                        contentAlignment = Alignment.Center,
                                    ) {
                                        AsyncImage(
                                            model = vm.dialogUiState.imageUrl,
                                            contentDescription = vm.dialogUiState.description,
                                            modifier = Modifier
                                                .size(240.dp, 240.dp)
                                                .padding(16.dp),
                                            //alignment = Alignment.Center
                                        )
                                        Row(Modifier.align(Alignment.TopEnd)) {
                                            FilledTonalIconButton(
                                                onClick = { /*TODO*/ },
                                                // TODO("colors and shapes")
                                            ) {
                                                Icon(Icons.Outlined.FavoriteBorder, null)
                                            }
                                            // TODO("colors and shapes")
                                            FilledTonalIconButton(onClick = {
                                                vm.showDishDialog = false
                                            }) {
                                                Icon(Icons.Default.Close, null)
                                            }
                                        }
                                    }
                                }

                                vm.dialogUiState.name?.let {
                                    Text(
                                        it,
                                        Modifier.padding(16.dp, 0.dp, 16.dp, 0.dp),
                                        style = MaterialTheme.typography.titleLarge,
                                        fontWeight = FontWeight.Bold
                                    )
                                }
                                Row(Modifier.padding(16.dp, 0.dp, 16.dp, 8.dp)) {
                                    Text(
                                        "${vm.dialogUiState.price} ₽ ",
                                        fontWeight = FontWeight.Bold
                                    )
                                    Text("${vm.dialogUiState.weight} г", color = Color.Gray)
                                }
                                vm.dialogUiState.description?.let {
                                    Text(
                                        it,
                                        Modifier.padding(16.dp, 0.dp, 16.dp, 8.dp)
                                    )
                                }
                                Button(
                                    onClick = {
                                        Log.v(
                                            TAG,
                                            "dialog: ${vm.dialogUiState.name} ${vm.dialogUiState.id}"
                                        )
                                        vm.dialogUiState.id?.let { dishId: Int ->
                                            Log.v(TAG, "dialog: $dishId")
                                            val x = vm.cart[dishId]
                                            if (x == null)
                                                vm.cart[dishId] = 1
                                            else
                                                vm.cart[dishId] = x + 1
                                        }
                                        vm.showDishDialog = false
                                    },
                                    Modifier
                                        .padding(16.dp, 0.dp, 16.dp, 16.dp)
                                        .fillMaxWidth()
                                ) {
                                    Text(stringResource(R.string.add_to_cart))
                                }
                            }
                        }
                    }
                }
            }
        }

        else -> Text("Wrong food category")
    }
    if (vm.showProgressIndicator)
        Box(Modifier.fillMaxSize(), Alignment.Center) {
            CircularProgressIndicator()
        }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OneDish(dish: DishesJson.Dish, showDialog: (Int) -> Unit) {
    Box(
//        {
//            Log.v(TAG, "clicked food category: id=${dish.id}, name=${dish.name}")
//
//        },
        modifier = Modifier
            .fillMaxWidth()
            .padding(4.dp, 2.dp, 4.dp, 2.dp)
    ) {

        Column {
            Card(
                {
                    Log.v(TAG, "OneDish: dish with ${dish.description} clicked")
                    dish.id?.let { i -> showDialog(i) }
                },
                Modifier
                    .fillMaxWidth()
                    .aspectRatio(1f, false),

                ) {
                val iUrl = dish.imageUrl ?: if (
                    URLUtil.isHttpsUrl(dish.description) &&
                    dish.description?.let { Patterns.WEB_URL.matcher(it).matches() } == true
                ) dish.description
                else
                    null
                Box(Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                    AsyncImage(
                        model =
                        //dish.imageUrl
                        iUrl,
                        contentDescription = dish.name,
                        //imageLoader = ElFoodApps.elFoodApps.newImageLoader()
                        modifier = Modifier.padding(8.dp),
                        //modifier = Modifier.fillMaxWidth().padding(16.dp),
                        //contentScale = ContentScale.FillWidth
                    )
                }
            }

            dish.name?.let {
                Text(
                    it,
                    Modifier
                        .padding(0.dp, 0.dp, 0.dp, 8.dp),
                    //            .weight(1f)
                    style = MaterialTheme.typography.titleSmall,
                )
                //    Spacer(modifier = Modifier.weight(1f))
            }
        }
    }
}

private const val TAG = "MenuScr"
