package ch.blossom.effood.dishes

import android.app.Application
import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.Snapshot
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import ch.blossom.effood.data.DataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

private const val TAG = "MenuViewModel"

class MenuViewModel(val apply: Application) : AndroidViewModel(apply) {

    var cart = DataRepository.cart

    var showDishDialog by mutableStateOf(false)
    private val _dialogUiState = MutableDishUiState()
    val dialogUiState: DishUiState = _dialogUiState

    fun fillDishDialog(i: Int){
        _dialogUiState.description=listOfDishes[i].description
        _dialogUiState.name=listOfDishes[i].name
        listOfDishes[i].id?.let { _dialogUiState.id=it }
        listOfDishes[i].weight?.let { _dialogUiState.weight=it }
        listOfDishes[i].price?.let { _dialogUiState.price=it }
        _dialogUiState.imageUrl=listOfDishes[i].imageUrl
        _dialogUiState.tegs=listOfDishes[i].tegs
    }

    val listOfDishes = DataRepository.listOfDishes
    val listOfTegs = DataRepository.listOfTegs

    //val t = listOfTegs.find{ t -> t.selected }?.teg
    //if(it.tegs?.contains(t)==true)
    var showProgressIndicator by mutableStateOf(false)
    private fun fetchJson() {
        if (listOfDishes.isNotEmpty())
            return
        showProgressIndicator = true
        viewModelScope.launch(Dispatchers.IO) {
            if (withContext(Dispatchers.Main) { listOfDishes.isEmpty() })
                DataRepository.refreshListOfDishes()

            withContext(Dispatchers.Default) {
                Snapshot.withMutableSnapshot {
                    showProgressIndicator = false
                }
            }
        }
    }

    init {
        fetchJson()
    }
}

@Stable
interface DishUiState {
    val description: String?
    val imageUrl: String?
    val id: Int?
    val price: Int?
    val weight: Int?
    val name: String?
    val tegs: List<String?>?
}

class MutableDishUiState() : DishUiState {
    override var description: String? by mutableStateOf(null)
    override var imageUrl: String? by mutableStateOf(null)
    override var id: Int by mutableStateOf(-1)
    override var price: Int by mutableStateOf(-1)
    override var weight: Int by mutableStateOf(-1)
    override var name: String? by mutableStateOf(null)
    override var tegs: List<String?>?= mutableStateListOf()
}

