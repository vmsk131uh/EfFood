package ch.blossom.fetch

import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.android.Android
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.logging.ANDROID
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.get
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.Serializable

import kotlinx.serialization.SerialName


@Serializable
data class CategoriesJson(
    @SerialName("сategories")
    val categories: List<Category?>? = null
) {
    @Serializable
    data class Category(
        @SerialName("id")
        val id: Int? = null,
        @SerialName("image_url")
        val imageUrl: String? = null,
        @SerialName("name")
        val name: String? = null
    )
}

@Serializable
data class DishesJson(
    @SerialName("dishes")
    val dishes: List<Dish?>? = null
) {
    @Serializable
    data class Dish(
        @SerialName("description")
        val description: String? = null,
        @SerialName("id")
        val id: Int? = null,
        @SerialName("image_url")
        val imageUrl: String? = null,
        @SerialName("name")
        val name: String? = null,
        @SerialName("price")
        val price: Int? = null,
        @SerialName("tegs")
        val tegs: List<String?>? = null,
        @SerialName("weight")
        val weight: Int? = null
    )
}

class FetchJson {
    companion object {

        private const val TAG = "FetchJson"

        private val ktorClient = HttpClient(Android) {
            engine {
                threadsCount = 1
                socketTimeout = 2000
                connectTimeout = 2000
            }
            install(ContentNegotiation) {
                json()
            }
            install(Logging) {
                level = LogLevel.ALL
                logger = Logger.ANDROID
            }
        }

        suspend fun fetchCategory(): FetchResponse {
            val response: CategoriesJson = try {
                ktorClient.get("https://run.mocky.io/v3/058729bd-1402-4578-88de-265481fd7d54").body()
            } catch (e: Exception){
                println( "fetchCategory: ${e.message} ${e.javaClass.simpleName}")
                e.printStackTrace()
                return FetchResponse.Error(e.javaClass.simpleName,e.message.orEmpty())
            }
            return FetchResponse.Success(response.categories)
        }

                suspend fun fetchDishes(): FetchResponse {
            val response: DishesJson = try {
                ktorClient.get("https://run.mocky.io/v3/c7a508f2-a904-498a-8539-09d96785446e").body()
            } catch (e: Exception){
                println( "fetchCategory: ${e.message} ${e.javaClass.simpleName}")
                e.printStackTrace()
                return FetchResponse.Error(e.javaClass.simpleName,e.message.orEmpty())
            }
            return FetchResponse.Success(response.dishes)
        }


    }




}

sealed class FetchResponse {
    data class Success<T>(var resp: T) : FetchResponse()
    data class HtmlError(
        var code: Int,
        var message: String = "",
        var body: String = ""
    ) : FetchResponse()
    data class Error(var exception: String = "", var message: String = "") : FetchResponse()
}
